LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

#LOCAL_CFLAGS := -DCOCOS2D_DEBUG=1
LOCAL_CPP_FEATURES := exceptions

LOCAL_ARM_MODE := arm

# Warning flags.
# LOCAL_CFLAGS += -Wdouble-promotion -Wignored-qualifiers -Wundef
LOCAL_CFLAGS := -Wchar-subscripts -Wcomment -Wformat=2 -Wnonnull -Winit-self
LOCAL_CFLAGS += -Wmain -Wmissing-braces -Wmissing-include-dirs -Wparentheses -Wsequence-point
LOCAL_CFLAGS += -Wreturn-type -Waddress -Wmissing-declarations -Wmissing-field-initializers
LOCAL_CFLAGS += -Wunused-function -Wunused-label -Wuninitialized

LOCAL_MODULE := hp_static

LOCAL_MODULE_FILENAME := libhp

# FILE_LIST_BEGIN

INCREMENT_BUILD := \
../../Classes/HPData.cpp \
../../Classes/HPDialog.cpp \
../../Classes/HPDialogManager.cpp \
../../Classes/HPEvent.cpp \
../../Classes/HPUtils.cpp

# FILE_LIST_END

UNIVERSAL_BUILD := HP-Everything.cpp

LOCAL_SRC_FILES := $(INCREMENT_BUILD)
                   
LOCAL_C_INCLUDES := \
$(LOCAL_PATH)/../../Classes \
$(LOCAL_PATH)/../../cocos2d \
$(LOCAL_PATH)/../../cocos2d/cocos \
$(LOCAL_PATH)/../../cocos2d/cocos/2d \
$(LOCAL_PATH)/../../cocos2d/cocos/editor-support \
$(LOCAL_PATH)/../../cocos2d/cocos/ui \
$(LOCAL_PATH)/../../cocos2d/cocos/platform/android/jni

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/../../Classes

include $(BUILD_STATIC_LIBRARY)