//
//  HPDialogManager.hpp
//  hp-library
//
//  Created by HuyPhan on 3/18/16.
//
//

#ifndef HPDialogManager_hpp
#define HPDialogManager_hpp

#include "HPMacro.hpp"
#include "HPHeader.hpp"
#include <functional>
#include <vector>

NS_HP_BEGIN
class HPDialog;

class HPDialogManager {
public:
    static HPDialogManager* getInstance();
    
    HPDialog* getCurrentDialog() const;
    void hideDialog();
    
    cocos2d::Node* getRunningNode();
    cocos2d::Node* getParentNode();
    cocos2d::Scene* getCurrentScene();
    
    std::vector<HPDialog*> getDialogs();
    
private:
    friend class HPDialog;
    
    void addToQueue(const std::function<void()>& callback);
    
    void pushDialog(cocos2d::Node* container, HPDialog* dialog, int localZOrder);
    void popDialog(HPDialog* dialog);
    
    void lock(HPDialog* dialog);
    void unlock(HPDialog* dialog);
    
    class Impl;
};
NS_HP_END

#endif /* HPDialogManager_hpp */
