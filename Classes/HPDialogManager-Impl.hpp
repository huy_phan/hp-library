//
//  HPDialogManager-Impl.hpp
//  hp-library
//
//  Created by HuyPhan on 3/18/16.
//
//

#ifndef HPDialogManager_Impl_hpp
#define HPDialogManager_Impl_hpp

#include "HPDialogManager.hpp"

#include <deque>
#include <memory>
#include <utility>
#include <vector>
#include "base/CCRefPtr.h"

NS_HP_BEGIN
class HPDialogManager::Impl : public HPDialogManager {
public:
    void updateCurrentScene();
    void processActionQueue();
    
    cocos2d::Scene* _lastScene;
    HPDialog* _lockingDialog;
    
    struct HPDialogInfo {
        HPDialogInfo(cocos2d::Node* _container, HPDialog* _dialog)
        : container(_container)
        , dialog(_dialog)
        {}
        
        cocos2d::RefPtr<cocos2d::Node> container;
        HPDialog* dialog;
    };
    
    std::deque<std::function<void()>> _actionQueue;
    std::vector<HPDialogInfo> _dialogStack;
};
NS_HP_END

#endif /* HPDialogManager_Impl_hpp */
