//
//  HPDialog.hpp
//  hp-library
//
//  Created by HuyPhan on 3/18/16.
//
//

#ifndef HPDialog_hpp
#define HPDialog_hpp

#include "HPMacro.hpp"
#include "HPHeader.hpp"
#include "2d/CCNode.h"

#include <functional>
#include <memory>
#include <utility>

NS_HP_BEGIN
class HPDialog : public cocos2d::Node {
public:
    enum class PopupDirection {
        TopDown     = 0,
        BottomUp    = 1,
        RightToLeft = 2,
        LeftToRight = 3,
    };
    
    using Callback0 = std::function<void()>;
    using Callback2 = std::function<void(cocos2d::Node*, HPDialog*)>;
    
    virtual void show(int localZOrder = 0);
    virtual void hide();
    
    /**
     * it will clear all event, register.
     */
    virtual void clear();
    
    virtual HPDialog* addShowBeganCallback(const Callback0& callback, int priority = 0);
    virtual HPDialog* addShowEndedCallback(const Callback2& callback, int priority = 0);
    virtual HPDialog* addHideBeganCallback(const Callback2& callback, int priority = 0);
    virtual HPDialog* addHideEndedCallback(const Callback0& callback, int priority = 0);
    
    virtual bool    usePopup() = 0;
    virtual const   PopupDirection popupDirection() = 0;
    virtual bool    useBlackBackground() = 0;
    virtual int     targetOpacity() { return 125; }
    
    virtual bool    useContainer() = 0;
    
    bool isHiding();
    bool isShowing();
    
protected:
    virtual void internalShow(int localZOrder);
    virtual void internalHide();
    
    HPDialog();
    virtual ~HPDialog();
    
    virtual bool init() override;
    virtual void onEnter() override;
    virtual void onExit() override;
    
    virtual void invokeShowBeganCallbacks();
    virtual void invokeShowEndedCallbacks();
    virtual void invokeHideBeganCallbacks();
    virtual void invokeHideEndedCallbacks();
    
private:
    class Impl;
    friend class Impl;
    std::unique_ptr<Impl> _impl;
};
NS_HP_END

#endif /* HPDialog_hpp */
