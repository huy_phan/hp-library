//
//  HPMacro.hpp
//  hp-library
//
//  Created by HuyPhan on 3/9/16.
//
//

#ifndef HPMacro_hpp
#define HPMacro_hpp

enum class TargetDevice {
    Auto,
    Iphone,
    IphoneHd,
    Ipad,
    IpadHd
};

#define NS_HP_BEGIN                 namespace hp {
#define NS_HP_END                   }

#define NS_DETAIL_BEGIN             namespace detail {
#define NS_DETAIL_END               }

/// Gets overloading function name.
#define GET_FUNCTION(name, arity) STRCAT(name, arity)

/// @name Cocos2d debug
/// @{
#if !defined(COCOS2D_DEBUG) || COCOS2D_DEBUG == 0
#if defined(_MSC_VER) || defined(__clang__) || defined(__GNUC__)
#   if defined(_MSC_VER)
#       define FUNCTION_SIGNATURE __FUNCSIG__
#   elif defined(__clang__) || defined(__GNUC__)
#       define FUNCTION_SIGNATURE __PRETTY_FUNCTION__
#   endif
#   define LOG_FUNC()                       do {} while (0)
#   define LOG_FUNC_FORMAT(format, ...)     do {} while (0)
#else
#   define LOG_FUNC()
#   define LOG_FUNC_FORMAT(format, ...)
#endif

#elif COCOS2D_DEBUG == 1
#if defined(_MSC_VER) || defined(__clang__) || defined(__GNUC__)
#   if defined(_MSC_VER)
#       define FUNCTION_SIGNATURE __FUNCSIG__
#   elif defined(__clang__) || defined(__GNUC__)
#       define FUNCTION_SIGNATURE __PRETTY_FUNCTION__
#   endif
#   define LOG_FUNC()                       CCLOG("%s.", FUNCTION_SIGNATURE)
#   define LOG_FUNC_FORMAT(format, ...)     CCLOG("%s: " format ".", FUNCTION_SIGNATURE, ## __VA_ARGS__)
#else
#   define LOG_FUNC()
#   define LOG_FUNC_FORMAT(format, ...)
#endif

#elif COCOS2D_DEBUG > 1
#if defined(_MSC_VER) || defined(__clang__) || defined(__GNUC__)
#   if defined(_MSC_VER)
#       define FUNCTION_SIGNATURE __FUNCSIG__
#   elif defined(__clang__) || defined(__GNUC__)
#       define FUNCTION_SIGNATURE __PRETTY_FUNCTION__
#   endif
#   define LOG_FUNC()                       CCLOG("%s.", FUNCTION_SIGNATURE)
#   define LOG_FUNC_FORMAT(format, ...)     CCLOG("%s: " format ".", FUNCTION_SIGNATURE, ## __VA_ARGS__)
#else
#   define LOG_FUNC()
#   define LOG_FUNC_FORMAT(format, ...)
#endif
#endif // COCOS2D_DEBUG

/// Concatenates two string literals.
#define STRCAT_HELPER(x, y)     x ## y
#define STRCAT(x, y)            STRCAT_HELPER(x, y)

/// Counts number of arguments.
#define COUNT_ARGS_HELPER(_0, _1, _2, _3, _4, _5, _6, _7, _8, _9, N, ...) N
#if defined(_MSC_VER)
#   define COUNT_ARGS_MSC(...)  COUNT_ARGS_HELPER __VA_ARGS__
#   define COUNT_ARGS(...)      COUNT_ARGS_MSC((0, ## __VA_ARGS__, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0))
#else
#   define COUNT_ARGS(...)      COUNT_ARGS_HELPER(0, ## __VA_ARGS__, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
#endif

/// Gets the first argument.
#define GET_FIRST_ARG_HELPER(arg, ...) arg
#define GET_FIRST_ARG(...) GET_FIRST_ARG_HELPER(__VA_ARGS__)

/// Gets all arguments except the first argument.
#define GET_EXCEPT_FIRST_ARG_HELPER(ignore, ...) __VA_ARGS__
#define GET_EXCEPT_FIRST_ARG(...) GET_EXCEPT_FIRST_ARG_HELPER(__VA_ARGS__)

#endif /* HPMacro_hpp */
