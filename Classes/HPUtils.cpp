//
//  HPUtils.cpp
//  hp-library
//
//  Created by HuyPhan on 3/9/16.
//
//

#include "HPUtils.hpp"
#include "cocos2d.h"
#include <vector>

NS_HP_BEGIN
void doRecursively(cocos2d::Node* node, const std::function<void(cocos2d::Node*)>& func);
std::vector<cocos2d::Node*> getChildren(cocos2d::Node* node);

void HPUtils::pauseAll(cocos2d::Node* node) {
    doRecursively(node, [](cocos2d::Node* current) { current->pause(); });
}

void HPUtils::resumeAll(cocos2d::Node* node) {
    doRecursively(node, [](cocos2d::Node* current) { current->resume(); });
}

std::vector<cocos2d::Node*> HPUtils::getDescendant(cocos2d::Node* node) {
    return getChildren(node);
}

void doRecursively(cocos2d::Node* node, const std::function<void(cocos2d::Node*)>& func) {
    func(node);
    for (auto&& child : node->getChildren()) {
        doRecursively(child, func);
    }
}

std::vector<cocos2d::Node*> getChildren(cocos2d::Node* node) {
    std::vector<cocos2d::Node*> ret;
    auto&& list = node->getChildren();
    if(list.empty()) return ret;
    
    for(auto&& iter : node->getChildren()) {
        ret.push_back(iter);
        
        // get list.
        auto&& temp = getChildren(iter);
        ret.insert(std::begin(ret), std::begin(temp), std::end(temp));
    }
    
    return ret;
}
NS_HP_END