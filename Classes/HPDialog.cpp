//
//  HPDialog.cpp
//  hp-library
//
//  Created by HuyPhan on 3/18/16.
//
//

#include "HPDialog-Impl.hpp"
#include "cocos2d.h"
#include "HPDialogManager.hpp"

NS_HP_BEGIN
HPDialog::HPDialog()
: _impl(new Impl(this)) {}

HPDialog::~HPDialog() {}

bool HPDialog::init() {
    return cocos2d::Node::init();
}

void HPDialog::onEnter() {
    cocos2d::Node::onEnter();
}

void HPDialog::onExit() {
    cocos2d::Node::onExit();
}

bool HPDialog::isHiding() {
    return _impl->_isHiding;
}
bool HPDialog::isShowing() {
    return _impl->_isShowing;
}

void HPDialog::show(int localZOrder/* = 0*/) {
    if (_impl->_isShowing == false) {
        _impl->_isShowing = true;
        HPDialogManager::getInstance()->addToQueue(CC_CALLBACK_0(HPDialog::internalShow, this, localZOrder));
    }
}

void HPDialog::hide() {
    if (_impl->_isHiding == false) {
        _impl->_isHiding = true;
        HPDialogManager::getInstance()->addToQueue(CC_CALLBACK_0(HPDialog::internalHide, this));
    }
}

void HPDialog::clear() {
    _impl->_hideBeganCallbacks.clear();
    _impl->_hideEndedCallbacks.clear();
    _impl->_showBeganCallbacks.clear();
    _impl->_showEndedCallbacks.clear();
}

HPDialog::Impl::Impl(HPDialog* base)
: _base(base)
, _isShowing(false)
, _isHiding(false) {}

void HPDialog::internalShow(int localZOrder) {
    LOG_FUNC();
    invokeShowBeganCallbacks();
    
    // handle call back event.
    auto callfunc   = cocos2d::CallFunc::create([this]{this->invokeShowEndedCallbacks();});
    cocos2d::Node* container = nullptr;
    
    // check need to use container -> mean create new object.
    if(useContainer()) {
        auto&& winSize = cocos2d::Director::getInstance()->getWinSize();
        cocos2d::Vec2 endPos(winSize.width * 0.5f, winSize.height * 0.5f);
        
        // create container
        container = cocos2d::Node::create();
        container->setContentSize(winSize);
        
        // add black background.
        if(useBlackBackground()) {
            auto layer = cocos2d::LayerColor::create(cocos2d::Color4B::BLACK);
            layer->setName("background");
            layer->setOpacity(0);
            layer->runAction(cocos2d::FadeTo::create(0.3f, (GLubyte)targetOpacity()));
            container->addChild(layer, -1);
        }
        
        // use popup moving.
        if(usePopup()) {
            // get begin position.
            cocos2d::Vec2 beginPos = getPosition();
            
            // detect direction begin position
            switch (popupDirection()) {
                case PopupDirection::TopDown:
                    beginPos.y = winSize.height * 1.5f;
                    beginPos.x = winSize.width * 0.5f;
                    break;
                case PopupDirection::BottomUp:
                    beginPos.y = -winSize.height * 1.5f;
                    beginPos.x = winSize.width * 0.5f;
                    break;
                case PopupDirection::LeftToRight:
                    beginPos.y = winSize.height * 0.5f;
                    beginPos.x = -winSize.width * 1.5f;
                    break;
                case PopupDirection::RightToLeft:
                    beginPos.y = winSize.height * 0.5f;
                    beginPos.x = winSize.width * 1.5f;
                    break;
            }
            
            // run animation
            setPosition(beginPos);
            auto move       = cocos2d::EaseElasticOut::create(cocos2d::MoveTo::create(0.3f, endPos), 1.0f);
            runAction(cocos2d::Sequence::createWithTwoActions(move, callfunc));
        }
        else {
            setPosition(endPos);
            runAction(callfunc);
        }
        container->addChild(this);
    }
    else {
        invokeShowEndedCallbacks();
        _impl->_isShowing = false;
    }
    
    HPDialogManager::getInstance()->pushDialog(container, this, localZOrder);
}

void HPDialog::internalHide() {
    LOG_FUNC();
    invokeHideBeganCallbacks();
    
    // callback event.
    auto callFunc = cocos2d::CallFunc::create([this] {
        retain();
        HPDialogManager::getInstance()->popDialog(this);
        invokeHideEndedCallbacks();
        release();
    });
    
    auto&& winSize = cocos2d::Director::getInstance()->getWinSize();
    
    // run to tranparent.
    if(useBlackBackground()) {
        auto layer = this->getParent()->getChildByName<cocos2d::LayerColor*>("background");
        if(layer) layer->runAction(cocos2d::FadeTo::create(0.3f, 0));
    }
    
    // check need to use container -> mean create new object.
    if(useContainer()) {
        // use popup.
        if(usePopup()) {
            // get begin position.
            cocos2d::Vec2 endPos = getPosition();
            
            // detect direction begin position
            switch (popupDirection()) {
                case PopupDirection::BottomUp:
                    endPos.y = winSize.height * 1.5f;
                    endPos.x = winSize.width * 0.5f;
                    break;
                case PopupDirection::TopDown:
                    endPos.y = -winSize.height * 1.5f;
                    endPos.x = winSize.width * 0.5f;
                    break;
                case PopupDirection::RightToLeft:
                    endPos.y = winSize.height * 0.5f;
                    endPos.x = -winSize.width * 1.5f;
                    break;
                case PopupDirection::LeftToRight:
                    endPos.y = winSize.height * 0.5f;
                    endPos.x = winSize.width * 1.5f;
                    break;
            }
            
            auto move = cocos2d::EaseElasticOut::create(cocos2d::MoveTo::create(0.3f, endPos), 1.0f);
            runAction(cocos2d::Sequence::createWithTwoActions(move, callFunc));
        }
        else {
            runAction(callFunc);
        }
    }
    else {
        HPDialogManager::getInstance()->popDialog(this);
        invokeHideEndedCallbacks();
        _impl->_isHiding = false;
    }
}

HPDialog* HPDialog::addShowBeganCallback(const Callback0& callback, int priority/* = 0*/) {
    _impl->_showBeganCallbacks.emplace_back(priority, callback);
    return this;
}

HPDialog* HPDialog::addShowEndedCallback(const Callback2& callback, int priority/* = 0*/) {
    _impl->_showEndedCallbacks.emplace_back(priority, callback);
    return this;
}

HPDialog* HPDialog::addHideBeganCallback(const Callback2& callback, int priority/* = 0*/) {
    _impl->_hideBeganCallbacks.emplace_back(priority, callback);
    return this;
}

HPDialog* HPDialog::addHideEndedCallback(const Callback0& callback, int priority/* = 0*/) {
    _impl->_hideEndedCallbacks.emplace_back(priority, callback);
    return this;
}

void HPDialog::Impl::invokeCallback0(std::vector<Callback0Info>& infos) {
    std::stable_sort(infos.begin(), infos.end(), [](const Callback0Info& lhs, const Callback0Info& rhs) {
        return lhs.first < rhs.first;
    });
    for (auto&& info : infos) {
        info.second();
    }
}
void HPDialog::Impl::invokeCallback2(std::vector<Callback2Info>& infos) {
    std::stable_sort(infos.begin(), infos.end(), [](const Callback2Info& lhs, const Callback2Info& rhs) {
        return lhs.first < rhs.first;
    });
    _base->retain();
    for (auto&& info : infos) {
        info.second(_base->getParent(), _base);
    }
    _base->release();
}

void HPDialog::invokeShowBeganCallbacks() {
    HPDialogManager::getInstance()->unlock(this);
    _impl->invokeCallback0(_impl->_showBeganCallbacks);
}

void HPDialog::invokeShowEndedCallbacks() {
    HPDialogManager::getInstance()->unlock(this);
    _impl->invokeCallback2(_impl->_showEndedCallbacks);}

void HPDialog::invokeHideBeganCallbacks() {
    HPDialogManager::getInstance()->unlock(this);
    _impl->invokeCallback2(_impl->_hideBeganCallbacks);
}

void HPDialog::invokeHideEndedCallbacks() {
    HPDialogManager::getInstance()->unlock(this);
    _impl->invokeCallback0(_impl->_hideEndedCallbacks);
}
NS_HP_END
