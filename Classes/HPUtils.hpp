//
//  HPUtils.hpp
//  hp-library
//
//  Created by HuyPhan on 3/9/16.
//
//

#ifndef HPUtils_hpp
#define HPUtils_hpp

#include "HPMacro.hpp"
#include "HPForward.hpp"
#include <sstream>
#include <vector>

NS_HP_BEGIN
template<class T, class... Args>
typename std::decay<T>::type max(T&& x, Args&&... args) {
    typename std::decay<T>::type ret = x;
    auto&& array = { args... };
    for (auto&& value : array)
        if (value > ret) {
            ret = value;
        }
    return ret;
}

template<class T, class... Args>
typename std::decay<T>::type min(T&& x, Args&&... args) {
    typename std::decay<T>::type ret = x;
    auto&& array = { std::forward<Args>(args)... };
    for (auto&& value : array)
        if (value < ret) {
            ret = value;
        }
    return ret;
}

// Termination function.
template<class T>
void toStringHelper(std::stringstream& ss, T&& value) {
    ss << value;
}

template<class T, class... Args>
void toStringHelper(std::stringstream& ss, T&& value, Args&&... args) {
    ss << std::forward<T>(value);
    toStringHelper(ss, std::forward<Args>(args)...);
}

template<class... Args>
std::string toString(Args&&... args) {
    static std::stringstream ss;
    ss.str(std::string());
    ss.clear();
    toStringHelper(ss, std::forward<Args>(args)...);
    return ss.str();
}

class HPUtils {
public:
    static void pauseAll(cocos2d::Node* node);
    static void resumeAll(cocos2d::Node* node);
    static std::vector<cocos2d::Node*> getDescendant(cocos2d::Node* node);
};
NS_HP_END

#endif /* HPUtils_hpp */
