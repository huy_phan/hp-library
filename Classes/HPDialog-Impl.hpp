//
//  HPDialog-Impl.hpp
//  hp-library
//
//  Created by HuyPhan on 3/18/16.
//
//

#ifndef HPDialog_Impl_hpp
#define HPDialog_Impl_hpp

#include "HPDialog.hpp"
#include <vector>
#include <map>
#include <utility>

NS_HP_BEGIN
class HPDialog::Impl {
public:
    Impl(HPDialog* base);
    
    bool _isShowing;
    bool _isHiding;
    
    HPDialog* _base;
    
    using Callback0Info = std::pair<int, Callback0>;
    using Callback2Info = std::pair<int, Callback2>;
    
    void invokeCallback0(std::vector<Callback0Info>& infos);
    void invokeCallback2(std::vector<Callback2Info>& infos);
    
    std::vector<Callback0Info> _showBeganCallbacks;
    std::vector<Callback2Info> _showEndedCallbacks;
    std::vector<Callback2Info> _hideBeganCallbacks;
    std::vector<Callback0Info> _hideEndedCallbacks;
};
NS_HP_END

#endif /* HPDialog_Impl_hpp */
