//
//  HPDialogManager.cpp
//  hp-library
//
//  Created by HuyPhan on 3/18/16.
//
//

#include "HPDialogManager-Impl.hpp"

#include "HPDialog.hpp"
#include "cocos2d.h"
#include "HPUtils.hpp"

NS_HP_BEGIN
HPDialogManager* HPDialogManager::getInstance() {
    static Impl sharedInstance;
    return &sharedInstance;
}

HPDialog* HPDialogManager::getCurrentDialog() const {
    auto impl = static_cast<const Impl*>(this);
    if(impl->_dialogStack.empty()) return nullptr;
    return impl->_dialogStack.back().dialog;
}

std::vector<HPDialog*> HPDialogManager::getDialogs() {
    std::vector<HPDialog*> ret;
    auto impl = static_cast<const Impl*>(this);
    if(impl->_dialogStack.empty()) return ret;
    for(auto&& iter : impl->_dialogStack) {
        ret.push_back(iter.dialog);
    }
    return ret;
}

cocos2d::Scene* HPDialogManager::getCurrentScene() {
    auto impl = static_cast<Impl*>(this);
    impl->updateCurrentScene();
    return impl->_lastScene;
}

void HPDialogManager::hideDialog() {
    LOG_FUNC();
    auto impl = static_cast<Impl*>(this);
    impl->_actionQueue.emplace_back([impl] {
        decltype(impl->_actionQueue) actions;
        actions.swap(impl->_actionQueue);
        impl->_dialogStack.back().dialog->hide();
        impl->_actionQueue.insert(impl->_actionQueue.cend(), actions.cbegin(), actions.cend());
        impl->processActionQueue();
    });
    impl->processActionQueue();
}

void HPDialogManager::pushDialog(cocos2d::Node* container, HPDialog* dialog, int localZOrder) {
    LOG_FUNC();
    // Retrieve impl pointer.
    auto impl = static_cast<Impl*>(this);
    auto parent = getRunningNode();
    
    HPUtils::pauseAll(parent);
    impl->_dialogStack.emplace_back(container, dialog);
    if(container) {
        parent->addChild(container, localZOrder);
    }
    else {
        HPUtils::resumeAll(dialog);
    }
}

void HPDialogManager::popDialog(HPDialog* dialog) {
    LOG_FUNC();
    auto impl = static_cast<Impl*>(this);
    auto parent = getParentNode();
    if(!impl->_dialogStack.empty()) {
        auto&& dialogInfo = impl->_dialogStack.back();
        if(dialogInfo.dialog && dialogInfo.container) dialogInfo.container->removeFromParent();
        impl->_dialogStack.pop_back();
    }
    
    HPUtils::resumeAll(parent);
}

void HPDialogManager::addToQueue(const std::function<void()>& callback) {
    auto impl = static_cast<Impl*>(this);
    impl->_actionQueue.emplace_back(callback);
    impl->processActionQueue();
}

void HPDialogManager::lock(HPDialog* dialog) {
    LOG_FUNC();
    auto impl = static_cast<Impl*>(this);
    
    impl->updateCurrentScene();
    impl->_lockingDialog = dialog;
}

void HPDialogManager::unlock(HPDialog* dialog) {
    LOG_FUNC();
    auto impl = static_cast<Impl*>(this);
    
    impl->_lockingDialog = nullptr;
    impl->processActionQueue();
}

cocos2d::Node* HPDialogManager::getRunningNode() {
    auto ptr = static_cast<Impl*>(this);
    ptr->updateCurrentScene();
    cocos2d::Node* ret = ptr->_lastScene;
    if (ptr->_dialogStack.empty() == false) {
        if(ptr->_dialogStack.back().container) {
            ret = ptr->_dialogStack.back().container;
        }
    }
    return ret;
}

cocos2d::Node* HPDialogManager::getParentNode() {
    auto ptr = static_cast<Impl*>(this);
    ptr->updateCurrentScene();
    cocos2d::Node* ret = ptr->_lastScene;
    if (ptr->_dialogStack.size() >= 2) {
        if(ptr->_dialogStack.at(ptr->_dialogStack.size() - 2).container) {
            ret = ptr->_dialogStack.at(ptr->_dialogStack.size() - 2).container;
        }
    }
    return ret;
}

void HPDialogManager::Impl::updateCurrentScene() {
    auto currentScene = cocos2d::Director::getInstance()->getRunningScene();
    auto transition = dynamic_cast<cocos2d::TransitionScene*>(currentScene);
    if (transition != nullptr) {
        currentScene = transition->getInScene();
    }
    if (currentScene != _lastScene) {
        _lastScene = currentScene;
        while (_dialogStack.empty() == false) {
            auto container = _dialogStack.back().container;
            if(container) container->removeFromParent();
            _dialogStack.pop_back();
        }
        _lockingDialog = nullptr;
    }
}

void HPDialogManager::Impl::processActionQueue() {
    if (_lockingDialog != nullptr) {
        return;
    }
    if (_actionQueue.empty() == false) {
        auto callback = _actionQueue.front();
        _actionQueue.pop_front();
        callback();
    }
}
NS_HP_END
