//
//  HPData.hpp
//  hp-library
//
//  Created by HuyPhan on 3/10/16.
//
//

#ifndef HPData_hpp
#define HPData_hpp

#include "HPMacro.hpp"
#include "HPNonCopyable.hpp"
#include "HPUtils.hpp"

#include <string>

NS_HP_BEGIN
template<class T>
void setInfoHelper(const char* key, const T& value);

template<class T>
T getInfoHelper(const char* key, const T& defaultValue);

NS_DETAIL_BEGIN
/// Single argument.
template<class T>
std::string createKey(T&& value) {
    return toString("___", std::forward<T>(value));
}

/// Multiple arguments.
template<class T, class... Args>
std::string createKey(T&& value, Args&&... args) {
    return toString("___", std::forward<T>(value), createKey(std::forward<Args>(args)...));
}

class HPDataBase : public HPNonCopyable {
public:
    const std::string& getKey() const;
    
protected:
    HPDataBase(std::string key) : _key(std::move(key)) {
        
    }
    
    HPDataBase(HPDataBase&&) = default;
    HPDataBase& operator=(HPDataBase&&) = delete;
    
    const std::string _key;
};
NS_DETAIL_END

template<class... Args>
class HPData;

// Zero format argument.
template<class T>
class HPData<T> : public detail::HPDataBase {
public:
    using ValueType = typename std::decay<T>::type;
    using FormatType = std::tuple<>;
    
    using FormatArgs [[deprecated("Use FormatType instead.")]] = FormatType;
    
    /**
     * Constructor.
     * @param key Unique key to store in user defaults.
     * @param defaultValue The default value in user defaults.
     */
    explicit HPData(std::string key, T defaultValue = T())
    : HPDataBase(std::move(key))
    , _defaultValue(std::move(defaultValue)) {}
    
    /**
     * Setter.
     * @param value The desired value to be set.
     */
    void set(T value) const {
        setInfoHelper<ValueType>(_key.c_str(), std::forward<T>(value));
    }
    
    /**
     * Getter.
     * @return Retrieved value.
     */
    ValueType get() const {
        return getInfoHelper<ValueType>(_key.c_str(), _defaultValue);
    }
    
    /**
     * Combination of getter and setter.
     */
    const HPData<T>& add(T value) const {
        set(get() + std::forward<T>(value));
        return *this;
    }
    
    /**
     * Implicit conversion.
     */
    operator ValueType() const {
        return get();
    }
    
    const HPData<T>& operator=(T value) const {
        set(std::forward<T>(value));
        return *this;
    }
    
    const HPData<T>& operator+=(T value) const {
        add(std::forward<T>(value));
        return *this;
    }
    
    const HPData<T>& operator-=(T value) const {
        add(-value);
        return *this;
    }
    
    ValueType operator++(int) const {
        auto ret = get();
        set(ret + 1);
        return ret;
    }
    
    ValueType operator--(int) const {
        auto ret = get();
        set(ret - 1);
        return ret;
    }
    
    const HPData<T>& operator++() const {
        add(+1);
        return *this;
    }
    
    const HPData<T>& operator--() const {
        add(-1);
        return *this;
    }
    
private:
    const T _defaultValue;
};

// Multiple format arguments.
template<class T, class... Args>
class HPData<T, Args...> : public detail::HPDataBase {
public:
    using ValueType = typename std::decay<T>::type;
    using FormatType = std::tuple<Args...>;
    
    using FormatArgs [[deprecated("Use FormatType instead.")]] = FormatType;
    
    explicit HPData(std::string key, T defaultValue = T())
    : HPDataBase(std::move(key))
    , _defaultValue(std::move(defaultValue)) {}
    
    void set(T value, Args... args) const {
        setInfoHelper<T>(toString(_key, detail::createKey(std::forward<Args>(args)...)).c_str(), std::forward<T>(value));
    }
    
    ValueType get(Args... args) const {
        return getInfoHelper<ValueType>(toString(_key, detail::createKey(std::forward<Args>(args)...)).c_str(), _defaultValue);
    }
    
    void add(T value, Args... args) const {
        auto&& current = get(args...);
        set(current + std::forward<T>(value), std::forward<Args>(args)...);
    }
    
    HPData<T> at(Args... args) const {
        return HPData<T>(toString(_key, detail::createKey(std::forward<Args>(args)...)).c_str(), _defaultValue);
    }
    
private:
    const T _defaultValue;
};

#define CREATE_DATA_INFO(...) GET_FUNCTION(CREATE_DATA_INFO_, COUNT_ARGS(__VA_ARGS__))(__VA_ARGS__)
#define CREATE_DATA_INFO_1(variableName)                variableName(# variableName)
#define CREATE_DATA_INFO_2(variableName, defaultValue)  variableName(# variableName, defaultValue)
NS_HP_END

#endif /* HPData_hpp */
