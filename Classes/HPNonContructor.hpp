//
//  HPNonContructor.hpp
//  hp-library
//
//  Created by HuyPhan on 3/9/16.
//
//

#ifndef HPNonContructor_hpp
#define HPNonContructor_hpp

#include "HPMacro.hpp"

NS_HP_BEGIN
class NonConstructible {
public:
    NonConstructible() = delete;
};
NS_HP_END

#endif /* HPNonContructor_hpp */
