//
//  HPForward.hpp
//  hp-library
//
//  Created by HuyPhan on 3/9/16.
//
//

#ifndef HPForward_hpp
#define HPForward_hpp

#include <bitset>
#include <cstdint>

namespace cocos2d {
    class Ref;
    class Node;
    class ClippingNode;
    class ClippingRectangleNode;
    class Scene;
    class Layer;
    class LayerColor;
    class Sprite;
    class SpriteFrame;
    class Image;
    class Label;
    class Value;
    class Vec2;
    class Vec3;
    using Point = Vec2;
    class __Array;
    using Array = __Array;
    class __Dictionary;
    using Dictionary = __Dictionary;
    class Event;
    class EventCustom;
    class EventDispatcher;
    class EventListener;
    class EventListenerCustom;
    class EventListenerKeyboard;
    class EventListenerTouchOneByOne;
    class EventListenerTouchAllAtOnce;
    class Touch;
    class Size;
    class ProgressTimer;
    class Rect;
    class ParticleSystemQuad;
    class Animation;
    class ActionEase;
    class ActionFloat;
    class ActionInterval;
    class Texture2D;
    struct Color3B;
    struct Color4B;
    struct Color4F;
    namespace extension {
        class ScrollView;
        class ControlButton;
    } // namespace extension
    namespace network {
        class HttpClient;
        class HttpResponse;
    } // namespace network
    namespace ui {
        class Scale9Sprite;
        class ListView;
        class LoadingBar;
        class ScrollView;
        class ListView;
        class Button;
    } // namespace ui
} // namespace cocos2d

namespace cocosbuilder {
    class NodeLoaderLibrary;
    class NodeLoader;
    class CCBReader;
} // namespace cocosbuilder

namespace CocosDenshion {
    class SimpleAudioEngine;
} // namespace CocosDenshion

namespace spine {
    class SkeletonAnimation;
} // namespace spine

#endif /* HPForward_hpp */
