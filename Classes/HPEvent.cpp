//
//  HPEvent.cpp
//  hp-library
//
//  Created by HuyPhan on 3/10/16.
//
//

#include "HPEvent.hpp"
#include "cocos2d.h"

NS_HP_BEGIN
NS_DETAIL_BEGIN
int HPEventBase::counter = 0;

HPEventBase::HPEventBase(std::string key)
: _id(counter++)
, _key(std::move(key))
{}

void HPEventBase::removeListeners() const {
    cocos2d::Director::getInstance()->getEventDispatcher()->removeCustomEventListeners(_key);
}

const std::string& HPEventBase::getKey() const {
    return _key;
}

cocos2d::EventListenerCustom* HPEventBase::addListenerHelper(const std::function<void(cocos2d::EventCustom*)>& callback) const {
    return cocos2d::Director::getInstance()->getEventDispatcher()->addCustomEventListener(toString(_id, _key), callback);
}

void HPEventBase::dispatchHelper(void* data) const {
    cocos2d::Director::getInstance()->getEventDispatcher()->dispatchCustomEvent(toString(_id, _key), data);
}
NS_DETAIL_END
NS_HP_END