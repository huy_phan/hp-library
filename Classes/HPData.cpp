//
//  HPData.cpp
//  hp-library
//
//  Created by HuyPhan on 3/10/16.
//
//

#include "HPData.hpp"
#include "cocos2d.h"

NS_HP_BEGIN
#pragma mark - SET INFO HELPER

template<> void setInfoHelper(const char* key, const bool& value) {
    cocos2d::UserDefault::getInstance()->setBoolForKey(key, value);
    cocos2d::UserDefault::getInstance()->flush();
}

template<> void setInfoHelper(const char* key, const int& value) {
    cocos2d::UserDefault::getInstance()->setIntegerForKey(key, value);
    cocos2d::UserDefault::getInstance()->flush();
}

template<> void setInfoHelper(const char* key, const std::string& value) {
    cocos2d::UserDefault::getInstance()->setStringForKey(key, value);
    cocos2d::UserDefault::getInstance()->flush();
}

template<> void setInfoHelper(const char* key, const float& value) {
    cocos2d::UserDefault::getInstance()->setFloatForKey(key, value);
    cocos2d::UserDefault::getInstance()->flush();
}

#pragma mark - GET INFO HELPER

template<> bool getInfoHelper(const char* key, const bool& defaultValue) {
    return cocos2d::UserDefault::getInstance()->getBoolForKey(key, defaultValue);
}

template<> int getInfoHelper(const char* key, const int& defaultValue) {
    return cocos2d::UserDefault::getInstance()->getIntegerForKey(key, defaultValue);
}

template<> std::string getInfoHelper(const char* key, const std::string& defaultValue) {
    return cocos2d::UserDefault::getInstance()->getStringForKey(key, defaultValue);
}

template<> float getInfoHelper(const char* key, const float& defaultValue) {
    return cocos2d::UserDefault::getInstance()->getFloatForKey(key, defaultValue);
}

NS_DETAIL_BEGIN
const std::string& HPDataBase::getKey() const {
    return _key;
}

NS_DETAIL_END
NS_HP_END