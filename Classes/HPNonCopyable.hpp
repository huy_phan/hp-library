//
//  HPHPNonCopyable.hpp
//  hp-library
//
//  Created by HuyPhan on 3/9/16.
//
//

#ifndef HPHPNonCopyable_hpp
#define HPHPNonCopyable_hpp

#include "HPMacro.hpp"

NS_HP_BEGIN
class HPNonCopyable {
public:
    HPNonCopyable() = default;
    HPNonCopyable(const HPNonCopyable&) = delete;
    HPNonCopyable(HPNonCopyable&&) = default;
    HPNonCopyable& operator=(const HPNonCopyable&) = delete;
    HPNonCopyable& operator=(HPNonCopyable&&) = default;
};
NS_HP_END

#endif /* HPHPNonCopyable_hpp */
