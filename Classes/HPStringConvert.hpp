//
//  HPStringConvert.hpp
//  hp-library
//
//  Created by HuyPhan on 3/9/16.
//
//

#ifndef HPStringConvert_hpp
#define HPStringConvert_hpp

#include "HPMacro.hpp"

NS_HP_BEGIN
// Compile-time string.
template<char... Cs>
struct CompileTimeString {
    static const char* value() {
        static const char a[sizeof...(Cs)] = { Cs... };
        return a;
    }
};

// Concatenates 2 strings.
template<class L, class R>
struct ConcatenateTwo;

template<char... Ls, char... Rs>
struct ConcatenateTwo<CompileTimeString<Ls...>, CompileTimeString<Rs...>> {
    using Result = CompileTimeString<Ls..., Rs...>;
};

// Concatenates N strings.
template<class... Cs>
struct Concatenate;

template<class C>
struct Concatenate<C> {
    using Result = C;
};

template<class C1, class C2>
struct Concatenate<C1, C2> {
    using Result = typename ConcatenateTwo<C1, C2>::Result;
};

template<class C, class ...Cs>
struct Concatenate<C, Cs...> {
    using Result = typename ConcatenateTwo<C, typename Concatenate<Cs...>::Result>::Result;
};
NS_HP_END


#endif /* HPStringConvert_hpp */
