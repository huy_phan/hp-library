//
//  HPEvent.hpp
//  hp-library
//
//  Created by HuyPhan on 3/10/16.
//
//

#ifndef HPEvent_hpp
#define HPEvent_hpp

#include "HPForward.hpp"
#include "HPFunction.hpp"
#include "HPMacro.hpp"
#include "HPUtils.hpp"
#include "HPNonCopyable.hpp"
#include "HPNonMovable.hpp"
#include "base/CCEventCustom.h"

#include <functional>
#include <memory>
#include <type_traits>

NS_HP_BEGIN
NS_DETAIL_BEGIN
class HPEventBase : public HPNonCopyable, public NonMovable {
public:
    void removeListeners() const;
    
    const std::string& getKey() const;
    
protected:
    HPEventBase(std::string key);
    
    cocos2d::EventListenerCustom* addListenerHelper(const std::function<void(cocos2d::EventCustom*)>& callback) const;
    
    void dispatchHelper(void* data) const;
    
    const int _id;
    const std::string _key;
    
    static int counter;
};
NS_DETAIL_END

// Multiple arguments.
template<class... Args>
class HPEvent : public detail::HPEventBase {
public:
    using DataType = std::tuple<Args...>;
    
    HPEvent(std::string key) : HPEventBase(std::move(key)) {}
    
    cocos2d::EventListenerCustom* addListener(const std::function<void(Args...)>& callback) const {
        return addListenerHelper([callback, this](cocos2d::EventCustom* event) {
            auto&& data = *static_cast<const DataType*>(event->getUserData());
            unpackTuple(typename SequenceGenerator<sizeof...(Args)>::Type(), data, callback);
        });
    }
    
    void dispatch(Args... args) const {
        auto&& data = DataType(std::forward<Args>(args)...);
        dispatchHelper(&data);
    }
    
private:
    template<std::size_t... Indices>
    void unpackTuple(Sequence<Indices...>, const DataType& data, const std::function<void(Args...)>& callback) const {
        callback(std::get<Indices>(data)...);
    }
};

// Zero argument.
template<>
class HPEvent<> : public detail::HPEventBase {
public:
    using DataType = std::tuple<>;
    
    HPEvent(std::string key) : HPEventBase(std::move(key)) {}
    
    cocos2d::EventListenerCustom* addListener(const std::function<void()>& callback) const {
        return addListenerHelper([callback](cocos2d::EventCustom*) {
            callback();
        });
    }
    
    void dispatch() const {
        dispatchHelper(nullptr);
    }
};

template<class... Args>
using Event [[deprecated("Use HPEvent instead.")]] = HPEvent<Args...>;

#define CREATE_EVENT_INFO(variableName) variableName(# variableName)
NS_HP_END

#endif /* HPEvent_hpp */
