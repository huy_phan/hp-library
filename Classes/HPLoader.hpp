//
//  HPLoader.hpp
//  hp-library
//
//  Created by HuyPhan on 3/9/16.
//
//

#ifndef HPLoader_hpp
#define HPLoader_hpp

#include "HPMacro.hpp"
#include "HPForward.hpp"

#include <tuple>
#include <type_traits>
#include <functional>

NS_HP_BEGIN
template<class NodeType, class ParentLoader, class... Ts>
class HPLoader : public ParentLoader {
public:
    template<class... Us>
    static HPLoader* loader(Us&&... args) {
        auto result = new (std::nothrow) HPLoader();
        if (result != nullptr) {
            result->autorelease();
            auto tuple = std::tuple<Us...>(std::forward<Us>(args)...);
            result->_callback = [result, data = std::move(tuple)] {
                using Indices = std::make_index_sequence<std::tuple_size<decltype(data)>::value>;
                return result->internalCreateNode(std::integral_constant<bool, sizeof...(Ts) == 0>(), Indices(), data);
            };
        } else {
            delete result;
            result = nullptr;
        }
        return result;
    }
    
private:
    using CreatorCallback = std::function<NodeType*()>;
    
    virtual NodeType* createNode(cocos2d::Node* parent, cocosbuilder::CCBReader* reader) {
        return _callback();
    }
    
    template<std::size_t... Indices, class DataType>
    NodeType* internalCreateNode(std::true_type, std::index_sequence<Indices...>, DataType&& data) {
        return NodeType::create(std::get<Indices>(data)...);
    }
    
    template<std::size_t... Indices, class DataType>
    NodeType* internalCreateNode(std::false_type, std::index_sequence<Indices...>, DataType&& data) {
        return NodeType::template create<Ts...>(std::get<Indices>(data)...);
    }
    
    CreatorCallback _callback;
};
NS_HP_END

#endif /* HPLoader_hpp */