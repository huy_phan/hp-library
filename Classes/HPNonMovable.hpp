//
//  HPNonMovable.hpp
//  hp-library
//
//  Created by HuyPhan on 3/9/16.
//
//

#ifndef HPNonMovable_hpp
#define HPNonMovable_hpp

#include "HPMacro.hpp"

NS_HP_BEGIN
class NonMovable {
public:
    NonMovable() = default;
    NonMovable(const NonMovable&) = default;
    NonMovable(NonMovable&&) = delete;
    NonMovable& operator=(const NonMovable&) = default;
    NonMovable& operator=(NonMovable&&) = delete;
};
NS_HP_END

#endif /* HPNonMovable_hpp */
